/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/25 09:16:14 by tkekae            #+#    #+#             */
/*   Updated: 2018/05/31 16:45:22 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *source, int c, size_t n)
{
	unsigned char	new_c;
	unsigned char	*dst_ptr;
	unsigned char	*src_ptr;
	size_t			i;

	i = 0;
	dst_ptr = (unsigned char *)dest;
	src_ptr = (unsigned char *)source;
	new_c = (unsigned char)c;
	while (i < n)
	{
		if (src_ptr[i] != new_c)
			dst_ptr[i] = src_ptr[i];
		else
		{
			dst_ptr[i] = src_ptr[i];
			i++;
			return (&dst_ptr[i]);
		}
		i++;
	}
	return (NULL);
}
