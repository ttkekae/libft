/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/29 12:40:01 by tkekae            #+#    #+#             */
/*   Updated: 2018/05/31 16:38:04 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char	*dst_ptr;
	char	*src_ptr;

	dst_ptr = (char *)dst;
	src_ptr = (char*)src;
	if (dst >= src)
	{
		while (len)
		{
			len--;
			dst_ptr[len] = src_ptr[len];
		}
	}
	else
		ft_memcpy(dst_ptr, src_ptr, len);
	return (dst_ptr);
}
