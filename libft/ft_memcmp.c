/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/29 11:15:39 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/04 14:41:45 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *str1, const void *str2, size_t len)
{
	size_t			i;
	size_t			j;
	unsigned char	*temp1;
	unsigned char	*temp2;

	i = 0;
	j = 0;
	temp1 = (unsigned char *)str1;
	temp2 = (unsigned char *)str2;
	while (j < len)
	{
		if (temp1[i] == temp2[i])
			i++;
		j++;
	}
	if (i == len)
		return (0);
	else
		return (temp1[i] - temp2[i]);
}
