/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/21 08:43:39 by tkekae            #+#    #+#             */
/*   Updated: 2018/05/31 17:48:43 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *ptr, int c, size_t len)
{
	char	*newptr;
	size_t	i;

	newptr = (char *)ptr;
	i = 0;
	while (i < len)
	{
		*newptr = (unsigned char)c;
		i++;
		newptr++;
	}
	return (ptr);
}
