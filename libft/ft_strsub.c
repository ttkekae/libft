/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/01 11:43:21 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/01 18:07:00 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *str, unsigned int start, size_t len)
{
	char	*fresh;
	size_t	i;

	i = 0;
	if (str == NULL)
		return (NULL);
	fresh = ft_memalloc(len + 1);
	if (fresh == NULL)
		return (NULL);
	while (i < len)
	{
		fresh[i] = str[start];
		i++;
		start++;
	}
	fresh[i] = '\0';
	return (fresh);
}
