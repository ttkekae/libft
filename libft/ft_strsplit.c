/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/04 08:14:58 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/12 17:28:58 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int		ft_wc(const char *str, char c)
{
	int i;
	int count;

	i = 0;
	count = 0;
	while (str[i])
	{
		if (str[i] != c)
		{
			while (str[i] != c && str[i])
				i++;
			count++;
		}
		i++;
	}
	return (count);
}

char			**ft_strsplit(char	const *s, char c)
{
	int		index[2];
	int		count;
	char	**wd;

	index[0] = 0;
	index[1] = 0;
	if (s == NULL || !(wd = (char **)malloc(sizeof(char *) * ft_wc(s, c) + 1)))
		return (NULL);
	while (s[index[0]])
	{
		count = 0;
		if (s[index[0]] != c)
		{
			while (s[index[0]] != c && s[index[0]])
			{
				index[0]++;
				count++;
			}
			wd[index[1]++] = ft_strsub(s, index[0] - count, count);
		}
		else
			index[0]++;
	}
	wd[index[1]] = 0;
	return (wd);
}
