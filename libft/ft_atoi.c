/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/23 09:29:47 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/16 15:00:19 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int		ft_whitespace(int i)
{
	return ((i == '\t' || i == '\r' || i == '\v' ||
				i == '\n' || i == '\f') || i == ' ');
}

int				ft_atoi(const char *str)
{
	int		sign;
	int		i;
	long	result;

	result = 0;
	sign = 1;
	i = 0;
	while (ft_whitespace(str[i]))
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			sign *= -1;
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9' && str[i] != '\0')
	{
		result = result * 10 + (str[i] - 48);
		if (result < 0 && sign == -1)
			return (0);
		if (result < 0 && sign == 1)
			return (-1);
		i++;
	}
	return (sign * result);
}
