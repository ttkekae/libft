/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/30 18:06:31 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/11 16:50:51 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *str)
{
	int		size;
	int		i;
	char	*fresh;

	if (str == NULL)
		return (NULL);
	size = ft_strlen(str) - 1;
	i = 0;
	while (str[i] == ' ' || str[i] == '\n' || str[i] == '\t')
		i++;
	while ((str[size] == ' ' || str[size] == '\n' || str[size] == '\t')
	&& (i < size))
		size--;
	fresh = ft_strsub(str, i, (size - i) + 1);
	if (fresh == NULL)
		return (NULL);
	return (fresh);
}
