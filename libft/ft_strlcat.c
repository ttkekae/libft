/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tkekae <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/24 10:51:04 by tkekae            #+#    #+#             */
/*   Updated: 2018/06/04 11:07:19 by tkekae           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	char	*temp;
	size_t	i;
	size_t	new_size;
	int		j;
	size_t	total;

	i = 0;
	temp = NULL;
	temp = dst;
	i = ft_strlen(dst);
	if (i > size)
		return (size + ft_strlen(src));
	j = 0;
	new_size = ft_strlen(src);
	total = i + new_size;
	while (i < (size - 1) && src[j])
	{
		temp[i] = src[j];
		i++;
		j++;
	}
	temp[i] = '\0';
	return (total);
}
